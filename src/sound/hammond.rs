use crate::sound::wave_form::WaveForm;

#[derive(Debug, PartialEq)]
pub struct Harmonic {
    pub index: usize,
    pub volume: f32,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Harmonics {
    pub harmonics: [f32; 8],
}

impl Default for Harmonics {
    fn default() -> Self {
        Harmonics::new(&[1.0])
    }
}

impl Harmonics {
    pub fn new(slice: &[f32]) -> Harmonics {
        let mut harmonics = Harmonics {
            harmonics: [0.0; 8],
        };
        for (index, volume) in slice.iter().enumerate() {
            harmonics.set_harmonic(Harmonic {
                index,
                volume: *volume,
            });
        }
        harmonics
    }

    pub fn set_harmonic(&mut self, Harmonic { index, volume }: Harmonic) {
        if index < self.harmonics.len() {
            self.harmonics[index] = volume;
        }
    }

    pub fn wave_form(&self, size: usize) -> WaveForm {
        WaveForm::from_function(
            move |phase| {
                let mut result = 0.0;
                for (index, volume) in self.harmonics.iter().enumerate() {
                    result += (phase * (index + 1) as f32).sin() * volume;
                }
                result
            },
            size,
        )
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn assert_eq_wave_form<F>(a: WaveForm, b: F)
    where
        F: Fn(f32) -> f32,
    {
        let b_wave_form = WaveForm::from_function(b, a.table.len());
        assert_eq!(a, b_wave_form);
    }

    #[test]
    fn first_harmonic_produces_sine_wave() {
        let wave_form = Harmonics::default().wave_form(10000);
        assert_eq_wave_form(wave_form, |x| x.sin());
    }

    #[test]
    fn second_harmonic_is_an_octave() {
        let wave_form = Harmonics::new(&[0.0, 1.0]).wave_form(10000);
        assert_eq_wave_form(wave_form, |x| (x * 2.0).sin());
    }

    #[test]
    fn sine_waves_are_summed_up() {
        let wave_form = Harmonics::new(&[1.0, 1.0]).wave_form(10000);
        assert_eq_wave_form(wave_form, |x| x.sin() + (x * 2.0).sin());
    }

    #[test]
    fn harmonics_can_be_fractions() {
        let wave_form = Harmonics::new(&[0.5]).wave_form(10000);
        assert_eq_wave_form(wave_form, |x| x.sin() * 0.5);
    }

    #[test]
    fn supports_at_least_8_harmonics() {
        let harmonics = Harmonics::new(&[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]);
        let wave_form = harmonics.wave_form(10000);
        assert_eq_wave_form(wave_form, |x| (x * 8.0).sin());
    }
}
