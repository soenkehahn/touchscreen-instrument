#![cfg_attr(not(feature = "dev"), deny(warnings))]
#![cfg_attr(feature = "dev", allow(warnings))]
#![allow(clippy::float_cmp)]

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;
#[macro_use]
extern crate custom_derive;
#[macro_use]
extern crate enum_derive;

mod areas;
mod cli;
mod evdev;
mod sound;
mod utils;

use anyhow::anyhow;
use anyhow::Result;
use areas::layouts::{grid, grid2, parallelograms};
use areas::{note_event_source::NoteEventSource, Areas};
use evdev::*;
use sound::audio_player::AudioPlayer;
use sound::midi_player::MidiPlayer;
use sound::Player;
use std::clone::Clone;
use std::fmt::Debug;

const TOUCH_WIDTH: i32 = 16383;
const TOUCH_HEIGHT: i32 = 9570;

fn get_binary_name() -> Result<String> {
    let current_exe = std::env::current_exe()?;
    let binary_name = current_exe
        .file_name()
        .ok_or(anyhow!("invalid current executable"))?
        .to_str()
        .ok_or(anyhow!("executable not valid unicode"))?;
    Ok(binary_name.to_string())
}

custom_derive! {
#[derive(Debug, Clone, Copy, IterVariants(LayoutTypeVariants), PartialEq, Default)]
    pub enum LayoutType {
        Parallelograms,
        Grid,
        #[default]
        Grid2,
    }
}

fn get_areas(layout_type: LayoutType) -> Areas {
    match layout_type {
        LayoutType::Parallelograms => parallelograms(TOUCH_WIDTH, TOUCH_HEIGHT),
        LayoutType::Grid => grid(TOUCH_WIDTH, TOUCH_HEIGHT, 16, 11, 36),
        LayoutType::Grid2 => grid2(TOUCH_WIDTH, TOUCH_HEIGHT),
    }
}

fn get_note_event_source(cli_args: &cli::Args) -> Result<NoteEventSource> {
    let areas = get_areas(cli_args.layout_type);
    areas.clone().spawn_ui(cli_args);
    let touches = if cli_args.dev_mode {
        TouchStateSource::blocking()
    } else {
        TouchStateSource::new("/dev/input/by-id/usb-ILITEK_Multi-Touch-V5100-event-if00")?
    };
    Ok(NoteEventSource::new(areas, touches))
}

fn get_player(cli_args: &cli::Args) -> Result<Box<dyn Player>> {
    if cli_args.midi {
        Ok(Box::new(MidiPlayer::new()?))
    } else {
        Ok(Box::new(AudioPlayer::new(cli_args)?))
    }
}

fn main() -> Result<()> {
    let cli_args = &cli::parse(get_binary_name()?, std::env::args())?;
    let note_event_source = get_note_event_source(cli_args)?;
    let player = get_player(cli_args)?;
    player.consume(note_event_source);
    Ok(())
}
