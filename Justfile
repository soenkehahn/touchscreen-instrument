dev:
  cargo test --all --color=always --features dev -- --test-threads=1 --quiet

ci: test build clippy fmt doc

test:
  cargo test --all --color=always -- --test-threads=1 --quiet

build:
  cargo build

clippy:
  cargo clippy --all-targets

fmt:
  cargo fmt -- --check

doc:
  cargo doc

install:
  cargo install --path . --force --locked

raspberry_debug: raspberry_build raspberry_debug_run

raspberry_build:
  cargo build --release

raspberry_debug_run:
  ./raspberry_debug.sh

raspberry_debug_stop:
  pkill xinit

raspberry_deploy:
  (cd ansible && ansible-playbook -i hosts tasks.yaml)
